
set search_path [list . <path_to_folder_containing_.lib_and_.db_files> ]
set target_library [list <library.db>]
set link_library  [list * <library.db>]

read_verilog -netlist pure_netlist.v
set sub_clocks  {clk}
create_clock  -name clk -period 2.900 [get_ports $sub_clocks]
read_saif -input nova_bw_pli.saif  -instance_name "nova_tb/nova" -verbose
report_power > nova_dynamic_pure2p6_2p9.txt
report_saif -hier -missing -gate > saif_report
exit
