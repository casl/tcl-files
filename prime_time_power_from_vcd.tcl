set collection_result_display_limit -1 

set search_path [list . ../Report_Files /scratch/secure_microcontroller/55nm_multivt/libraries/ ]
set target_library [list  fsf0l_gls_generic_core_ff1p32vm40c.db]
set link_library  [list * fsf0l_gls_generic_core_ff1p32vm40c.db]
set power_enable_analysis "true"

read_verilog ../Report_Files/netlist.v
current_design mkRing
link
set sub_clocks  {CLK}
create_clock  -name CLK -period 1.000 [get_ports $sub_clocks]

read_vcd ../post_synthesis_simulation/dump.vcd -strip_path "top/tb/ring"
report_switching_activity -list_not_annotated > switching_activity
report_power -verbose -hierarchy > ful_power_report.txt
exit
