#******************** Following is what every user needs to change *********************#
set_attribute hdl_search_path {./ ../Verilog/ /tools/Bluespec-2014.07.A/lib/Verilog/}
set_attribute lib_search_path {/tools/std_libraries/ ./}
set_attribute library library_file.lib
#set_attribute information_level 6 

set myFiles [list all_verilog_files.v ]; # list of all verilog/vhdl files to be parsed.
set myTop mkTopModuleName;                           ;# Top Module name

#******************* Mostly no changes required beyond this point ************************#


read_hdl ${myFiles}
elaborate ${myTop}
read_sdc ./constraints.sdc
ungroup -all -flatten
synthesize -to_mapped -effort high
write_hdl -mapped > ./reports/${myTop}_netlist.v
report gates > ./reports/${myTop}_gates_report.txt
report timing > ./reports/${myTop}_timing_report.txt
report area > ./reports/${myTop}_area_report.txt
write_sdf -nonegchecks -precision 5 -setuphold split -edges check_edge -design ${myTop} -timescale ns > ./reports/${myTop}_sdf.sdf
