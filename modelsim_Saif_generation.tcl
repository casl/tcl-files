# To Generate the Backward SAIF file using the MODELSIM Simulation 

# Please add the following lines in your testbench 
# Specify the Top Module through hierarchy (Eg: testbench.Top_Module)

initial begin
    $set_gate_level_monitoring("ON");
    $set_toggle_region("testbench.DUT");
    $toggle_start;
    #10000000 $toggle_stop;
    $display("%t", $time);
    $toggle_report("Name_Saif_File.saif", 1e-12 ,"testbench.DUT");
end

# When running Modelsim, Use the following command
# Also add the path for libvpower.so location in your PC
# Change the Variables 
# test_fixture - Name of the testbench 
vsim -c -pli /afs/bp.ncsu.edu/dist/synopsys_syn/linux/power/vpower/libvpower.so work.testbench
run -all 


