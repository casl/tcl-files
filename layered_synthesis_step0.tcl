
set search_path [list .]
set target_library  "../../nova_virat/libraries/.db" 
set link_library  {* "../../nova_virat/libraries/.db"}

set rtl_image [list ../verilog/BitStream_buffer.v ../verilog/BitStream_controller.v ../verilog/BitStream_parser_FSM_gating.v ../verilog/CodedBlockPattern_decoding.v ../verilog/DF_mem_ctrl.v ../verilog/DF_pipeline.v ../verilog/DF_reg_ctrl.v ../verilog/DF_top.v ../verilog/IQIT.v ../verilog/Inter_mv_decoding.v ../verilog/Inter_pred_CPE.v ../verilog/Inter_pred_LPE.v ../verilog/Inter_pred_pipeline.v ../verilog/Inter_pred_reg_ctrl.v ../verilog/Inter_pred_sliding_window.v ../verilog/Inter_pred_top.v ../verilog/Intra4x4_PredMode_decoding.v ../verilog/Intra_pred_PE.v ../verilog/Intra_pred_pipeline.v ../verilog/Intra_pred_reg_ctrl.v ../verilog/Intra_pred_top.v ../verilog/NumCoeffTrailingOnes_decoding.v ../verilog/QP_decoding.v ../verilog/bitstream_gclk_gen.v ../verilog/bs_decoding.v ../verilog/cavlc_consumed_bits_decoding.v ../verilog/cavlc_decoder.v ../verilog/dependent_variable_decoding.v ../verilog/end_of_blk_decoding.v ../verilog/exp_golomb_decoding.v ../verilog/ext_RAM_ctrl.v ../verilog/heading_one_detector.v ../verilog/hybrid_pipeline_ctrl.v ../verilog/level_decoding.v ../verilog/nC_decoding.v ../verilog/nova.v ../verilog/nova_defines.v ../verilog/pc_decoding.v ../verilog/ram_async_1r_sync_1w.v ../verilog/ram_sync_1r_sync_1w.v ../verilog/rec_DF_RAM_ctrl.v ../verilog/rec_gclk_gen.v ../verilog/reconstruction.v ../verilog/run_decoding.v ../verilog/sum.v ../verilog/syntax_decoding.v ../verilog/timescale.v ../verilog/total_zeros_decoding.v ]

define_design_lib work -path ./synthesis/work

set hdlin_check_no_latch true
set hdlin_report_syn_cell true ; # no man entry for this command. Need to check
set hdlin_report_inferred_modules verbose
set hlo_resource_allocation "constraint_driven"

#set report_default_significant_digits [getenv SIGDIGIT];

# RCFNL
set_flatten true -minimize single_output -phase true;

set auto_wire_load_selection "false"
set compile_delete_unloaded_sequential_cells "false"
set compile_seqmap_enable_output_inversion "false"
set compile_seqmap_propagate_constants "false"
set compile_seqmap_propagate_high_effort "false"
set timing_enable_multiple_clocks_per_reg "true"
set compile_dont_use_dedicated_scanout 1
set enable_recovery_removal_arcs "false"
set compile_implementation_selection "true"
set write_name_nets_same_as_ports true
set verilogout_higher_designs_first true
set verilogout_no_tri true
set report_default_significant_digits 4
redirect analyze.log {analyze -f verilog $rtl_image} -tee

set_wire_load_model -name "wl0"
set wire_load_mode "top"
set hdlin_no_group_register true
# -----------------------------------------------------------------------------
# Elaborate the Design
# -----------------------------------------------------------------------------

redirect .elaborate.log {elaborate -architecture verilog nova } -tee 

  current_design nova
  link                       
set_isolate_ports -type buffer [all_outputs]
# RCFNL
#set_fix_multiple_port_nets -feedthroughs -outputs -constants [get_designs *];

# RCFNL
set_ultra_optimization true
set_structure -boolean true -boolean_effort high -timing true;

  uniquify -dont_skip_empty_designs  

  # Set constraints and mapping on target library



  set sub_clocks  {clk}
  set sub_resets  {reset_n}
  
create_clock  -name clk -period 3.200 [get_ports $sub_clocks] 

set_output_delay 0.100 -max -clock  clk [all_outputs]
set_input_delay 0.100 -max -clock  clk [remove_from_collection [all_inputs] [get_ports clk]]

set_ideal_network [get_ports clk]
set_clock_transition 0.100 [all_clocks]

current_design nova

set compile_auto_ungroup_override_wlm true
set_fix_multiple_port_nets -feedthroughs -outputs -constants

	set strategy "timing_high_effort_script"
  
group_path -from [all_inputs ] -name IN2REG
group_path -to [all_outputs ] -name REG2OUT
group_path -from [all_inputs ] -to [all_outputs ] -name FEEDTHRUS
set_wire_load_model -name "wl0"
set wire_load_mode "top"
set_max_area 0.0
redirect ./1strun/compile1.log {compile -map_effort high -area_effort high} -tee; #initial compile command for high timing optimization

set_wire_load_model -name "wl0"
set wire_load_mode "top"
ungroup -flatten -all

redirect ./1strun/nova_timing.rpt {report_timing -max 5 -capacitance -transition_time -nets -input_pins} -tee;
redirect ./1strun/nova_area.rpt {report_area} -tee;
redirect ./1strun/nova_qor.rpt {report_qor} -tee;
redirect ./1strun/nova_power.rpt {report_power} -tee;
redirect ./1strun/nova_constraints_pass_com1.rpt {report_constraints} -tee;

write -f verilog -hier -o ./1strun/pure_netlist.v
write_sdc ./1strun/nova.sdc
write_sdf ./1strun/nova.sdf
write -f ddc -h -o ./1strun/nova.ddc
report_net_fanout -high_fanout > 1strun/fanout_report
report_design > 1strun/design_report
write_saif -output 1strun/nova.saif


