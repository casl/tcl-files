set myClk CLK_signal_name;                                ;# Name of the Clock signal 
set myPeriod 3.000                                        ;# Clock period specified. Unit can be depending on the libraries used by you
set myDutycycle 0.50;

set_max_delay ${myPeriod} -from [all_inputs] -to [all_outputs]  ;# This is for max combo delay.
set waveform [list 0.0000 [expr ${myPeriod}*${myDutycycle}]]  ;#Comment out this line and the next line if you want to synthesize only combo blocks.
create_clock -name ${myClk} -period ${myPeriod} -waveform ${waveform} [get_ports ${myClk}]

############### Use these only if you know what it means . The above will work for a simple synthesis netlist ################

#set_clock_uncertainty [expr 0.07 * ${myPeriod}] [all_clocks]
#set_clock_uncertainty -hold 0.050 [all_clocks]
#set_input_delay [expr 0.30 * ${myPeriod}] [get_ports {all_inputs}] -clock [get_clocks ${myClk}]
#set_output_delay [expr 0.50 * ${myPeriod}] [get_ports {all_outputs}] -clock [get_clocks ${myClk}]
#
#set_load  0.005 [all_outputs]
#set_max_capacitance 0.05 [current_design]
#set_max_transition 0.35 [current_design]
#set_max_fanout 10 [current_design]
#set_clock_latency  0.5 [get_clocks ${myClk}]
