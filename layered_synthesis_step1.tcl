set collection_result_display_limit -1
history keep 200
set search_path [list .]
set target_library  "../../nova/nova_virat/libraries/.db" 
set link_library  {* "../../nova/nova_virat/libraries/.db"}

  set sub_clocks  {clk}
  set sub_resets  {reset_n}
define_design_lib work -path ./synthesis/work


read_verilog -netlist ./pure_netlist_3p1ns.v 
current_design nova
set_wire_load_model -name "wl0"
set wire_load_mode "top"
link

source dont_touch_NC
source remove_donttouch_flop_C
source remove_donttouch_flop_NC
source remove_donttouch_fanin_C
set_disable_timing [all_dont_touch -cells]
source disable_NC
source dont_touch_NC_flops
source false_path_NC

create_clock -name clk -period 2.600 [get_ports $sub_clocks]

set_output_delay 0.100 -max -clock  clk [all_outputs]
set_input_delay 0.100 -max -clock  clk [remove_from_collection [all_inputs] [get_ports clk]]

set_clock_transition 0.100 [all_clocks]
set_ideal_network [get_ports clk]
set_wire_load_model -name "wl0"
set wire_load_mode "top"
set_ultra_optimization true
set_max_area 0.0
#redirect ./2ndrun/compile1.log {compile_ultra -timing_high_effort_script} -tee; #initial compile command for high timing optimization
redirect ./layered_3p1ns_compile1.log {compile -incremental_mapping -area_effort high} -tee; #initial compile command for high timing optimization

redirect ./layered_3p1ns_timing.rpt {report_timing -max 1 -capacitance -transition_time -nets -input_pins} -tee;
redirect ./layered_3p1ns_area.rpt {report_area} -tee;
redirect ./layered_3p1ns_qor.rpt {report_qor} -tee;
redirect ./layered_3p1ns_power.rpt {report_power} -tee;
redirect ./layered_3p1ns_constraints.rpt {report_constraints} -tee;
write -f verilog -hier -o ./layered_3p1ns_netlist.v
write_sdc ./layered_3p1ns.sdc
write_sdf ./layered_3p1ns.sdf
write -f ddc -h -o ./layered_3p1ns.ddc
report_net_fanout -high_fanout > layered_3p1ns_fanout_report
report_design > layered_3p1ns_design_report
write_saif -output layered_3p1ns.saif
history > history
