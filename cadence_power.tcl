set_attribute hdl_search_path {./ ../Verilog/ /tools/Bluespec-2014.07.A/lib/Verilog/}
set_attribute lib_search_path {/tools/std_libraries/UMCIP/synopsys/ ./}
set_attribute library uk65lscllmvbbl_090c125_wc.lib
set_attribute information_level 6
read_hdl mknmr_netlist.v
elaborate mknmr
read_tcf -instance nmr_perceptron nmr.tcf
report power -detail > power_results.txt
