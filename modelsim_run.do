set IgnoreSVAWarning 1
set IgnoreWarning 1
set StdArithNoWarnings 1
set NumericStdNoWarnings 1
set IgnoreNote 1
set IgnoreError 1
set IgnoreFailure 1
set Show_VitalChecksWarnings 1
vlog *.v
vopt +acc TopModule -o opt_tb
vsim opt_tb -onfinish stop +no_glitch_msg +no_timing_msg
onbreak {
echo "Resume macro at $now"
resume
}
force -freeze sim:/nova_tb/clk 1 0, 0 {2500 ps} -r {5 ns}
force -freeze {sim:/nova_tb/nova/reconstruction/DF_top/DF_pipeline/\DF_edge_counter_TD_reg[3] /Q} X 0
run -all
resume

