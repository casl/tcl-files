# run the following command in the same place as this file is kept.:
#   irun *v. +access+r +override_timescale +define+functional -input tcf_dump.tcl 
# Give the module name and output tcf file dump name in line 6 of this file.


dumptcf -scope tb.nmr_perceptron -output nmr.tcf -overwrite
run 
dumptcf -end
exit

